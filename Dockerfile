FROM node:alpine
ADD . /app
WORKDIR /app
RUN npm ci
CMD ["npm", "run", "start"]