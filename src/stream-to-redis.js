const stream = require('package-stream');
const Redis = require('ioredis');
const { orderBy } = require('lodash');

module.exports.run = async () => {
  const redis = new Redis(process.env.REDIS_PORT || 56379, process.env.REDIS_HOST || 'localhost');

  const dbSize = await redis.dbsize();

  if (dbSize < 900000) { // eg we are already loaded into redis
    process.on('SIGINT', () => {
      redis.disconnect();
    });
    const registry = stream({
      db: 'https://replicate.npmjs.com',
      include_docs: true,
    });

    let packageCount = 0;
    const seenKeys = {};
    const keyCounts = {};
    await new Promise((resolve) => {
      registry.on('package', async (pkg) => {
        Object.keys(pkg).forEach((key) => {
          if (seenKeys[key] === undefined) {
            seenKeys[key] = pkg[key];
            keyCounts[key] = 0;
          }
          keyCounts[key] += 1;
        });

        redis.set(`package:${pkg.name}`, JSON.stringify(pkg, null, 2));

        packageCount += 1;
        if (packageCount % 1000 === 0) {
          console.log('redis:', packageCount);
          const counts = Object.keys(keyCounts).map(key => ({
            key,
            count: keyCounts[key],
          }));
          redis.set('results:seen-keys', JSON.stringify(seenKeys, null, 2));
          redis.set('results:key-counts', JSON.stringify(orderBy(counts, ['count'], ['desc']), null, 2));
        }
      }).on('up-to-date', async () => {
        resolve();
        redis.set('results:seen-keys', JSON.stringify(seenKeys, null, 2));
      });
    });
  } else {
    console.log('skipping redis as it is already populated!');
  }
  redis.disconnect();
};
