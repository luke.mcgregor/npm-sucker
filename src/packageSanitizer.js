
const Joi = require('joi');
const npmDownloadCounts = require('download-counts');

const personSchema = Joi.object({
  name: Joi.string(),
  email: Joi.string().email(),
});

const packageSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.alt(Joi.string(), Joi.any().strip()),
  created: Joi.alt(Joi.string().isoDate(), Joi.any().strip()),
  modified: Joi.alt(Joi.string().isoDate(), Joi.any().strip()),
  lastPublisher: Joi.alt(personSchema, Joi.any().strip()),
  homepage: Joi.alt(Joi.string().uri(), Joi.any().strip()),
  owners: Joi.alt(Joi.array().items(personSchema), Joi.any().strip()),
  keywords: Joi.alt(Joi.array().items(Joi.string()), Joi.any().strip()),
  version: Joi.alt(Joi.string(), Joi.any().strip()),
  versions: Joi.alt(Joi.array().items(Joi.string()), Joi.any().strip()),
  repository: Joi.alt(Joi.string().uri(), Joi.any().strip()),
  dependencies: Joi.alt(Joi.object().pattern(/^/, Joi.string()), Joi.any().strip()),
  devDependencies: Joi.alt(Joi.object().pattern(/^/, Joi.string()), Joi.any().strip()),
  peerDependencies: Joi.alt(Joi.object().pattern(/^/, Joi.string()), Joi.any().strip()),
  optionalDependencies: Joi.alt(Joi.object().pattern(/^/, Joi.string()), Joi.any().strip()),
  maintainers: Joi.alt(Joi.array().items(personSchema), Joi.any().strip()),
  starsCount: Joi.alt(Joi.number(), Joi.any().strip()),
  license: Joi.alt(Joi.string(), Joi.any().strip()),
  deprecated: Joi.alt(Joi.bool(), Joi.any().strip()),
  authors: Joi.alt(Joi.array().items(personSchema), Joi.any().strip()),
});

module.exports.sanitizePackage = async (pkg) => {
  const sanitized = await Joi.validate(pkg, packageSchema, {
    stripUnknown: { arrays: true, objects: true },
  });

  return { ...sanitized, averageDownloads: npmDownloadCounts[pkg.name] || 0 };
};

module.exports.stripComplex = async (pkg) => {
  const sanitized = await Joi.validate(pkg, Joi
    .object()
    .pattern(/^/,
      Joi.alt(
        Joi.string(),
        Joi.bool(),
        Joi.number(),
        Joi.any().strip(),
      )), { stripUnknown: true });

  return sanitized;
};
