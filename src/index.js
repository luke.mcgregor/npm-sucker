const streamToRedis = require('./stream-to-redis');
const redisToMongo = require('./redis-to-mongo');
const redisToNeo4j = require('./redis-to-neo4j');

(async () => {
  await streamToRedis.run();
  await redisToMongo.run();
  await redisToNeo4j.run();
  process.exit();
})();
