const neo4j = require('neo4j-driver').v1;
const Redis = require('ioredis');
const { sanitizePackage, stripComplex } = require('./packageSanitizer');

module.exports.run = async () => {
  const redis = new Redis(process.env.REDIS_PORT || 56379, process.env.REDIS_HOST || 'localhost');

  const neo4jDriver = neo4j.driver(
    `bolt://${process.env.NEO4J_HOST || 'localhost'}:${process.env.NEO4J_PORT || 57687}`,
    neo4j.auth.basic('neo4j', 'password'),
  );
  const erroredPackages = [];
  const indexSession = neo4jDriver.session();
  await indexSession.run('CREATE CONSTRAINT ON (p:Package) ASSERT p.name IS UNIQUE');
  await indexSession.run('CREATE CONSTRAINT ON (p:Person) ASSERT p.email IS UNIQUE');
  indexSession.close();
  const createNodes = async (key) => {
    const session = neo4jDriver.session();
    try {
      const pkg = await sanitizePackage(JSON.parse(await redis.get(key)));
      const simplePackage = await stripComplex(pkg);
      await session.run(
        'MERGE (p:Package{name: {package}.name}) ON CREATE SET p = {package} ON MATCH SET p += {package}',
        {
          package: simplePackage,
        },
      );
      const people = [
        ...(pkg.lastPublisher ? [pkg.lastPublisher] : []),
        ...(pkg.owners || []),
        ...(pkg.maintainers || []),
        ...(pkg.authors || []),
      ];
      await Promise.all(people.filter(x => x.email).map(person => session.run(
        'MERGE (o:Person {email: {person}.email}) ON CREATE SET o = {person} ON MATCH SET o += {person}',
        { person },
      )));
    } catch (ex) {
      console.error(ex);
      erroredPackages.push(key);
    } finally {
      session.close();
    }
  };
  const createRelationships = async (key) => {
    const session = neo4jDriver.session();
    try {
      const pkg = await sanitizePackage(JSON.parse(await redis.get(key)));
      const dependencies = pkg.dependencies || {};
      await Promise.all(Object.keys(dependencies || {}).map(dep => session.run(
        `MERGE (p:Package {name: $packageName})
         MERGE (d:Package {name: $dependencyName}) 
         MERGE (p) -[:DEPENDS { version: $dependencyVersion }] -> (d)`,
        {
          dependencyName: dep,
          dependencyVersion: dependencies[dep],
          packageName: pkg.name,
        },
      )));
      const peerDependencies = pkg.peerDependencies || {};
      await Promise.all(Object.keys(peerDependencies || {}).map(dep => session.run(
        `MERGE (p:Package {name: $packageName})
         MERGE (d:Package {name: $dependencyName}) 
         MERGE (p) -[:DEPENDS { version: $dependencyVersion }] -> (d)`,
        {
          dependencyName: dep,
          dependencyVersion: peerDependencies[dep],
          packageName: pkg.name,
        },
      )));
      const devDependencies = pkg.devDependencies || {};
      await Promise.all(Object.keys(devDependencies).map(dep => session.run(
        `MERGE (p:Package {name: $packageName})
         MERGE (d:Package {name: $dependencyName}) 
         MERGE (p) -[:DEPENDS_DEV { version: $dependencyVersion }] -> (d)`,
        {
          dependencyName: dep,
          dependencyVersion: devDependencies[dep],
          packageName: pkg.name,
        },
      )));
      const owners = pkg.owners || [];
      await Promise.all(owners.filter(x => x.email).map(owner => session.run(
        `MATCH (p:Package {name: $packageName})
         MERGE (o:Person {email: {owner}.email}) ON CREATE SET o = {owner} ON MATCH SET o += {owner}
         MERGE (o) -[:OWNS] -> (p)`,
        {
          owner: {
            name: owner.name,
            email: owner.email,
          },
          packageName: pkg.name,
        },
      )));
    } catch (ex) {
      console.error(ex);
      erroredPackages.push(key);
    } finally {
      session.close();
    }
  };
  let packageCount = 0;
  await new Promise((resolve) => {
    const stream = redis.scanStream({ match: 'package:*', count: 100 });
    let lastPrint = 0;
    stream.on('data', (resultKeys) => {
      stream.pause();

      Promise.all(resultKeys.map(createNodes)).then(() => {
        stream.resume();
      });
      packageCount += resultKeys.length;
      if (packageCount >= lastPrint + 1000) {
        console.log('neo4j-nodes: ', packageCount);
        lastPrint = packageCount;
      }
    });

    stream.on('end', () => {
      resolve();
    });
  });

  packageCount = 0;
  await new Promise((resolve) => {
    const stream = redis.scanStream({ match: 'package:*', count: 100 });
    let lastPrint = 0;
    stream.on('data', (resultKeys) => {
      stream.pause();

      Promise.all(resultKeys.map(createRelationships)).then(() => {
        stream.resume();
      });
      packageCount += resultKeys.length;
      if (packageCount >= lastPrint + 1000) {
        console.log('neo4j-relationships: ', packageCount);
        lastPrint = packageCount;
      }
    });

    stream.on('end', () => {
      resolve();
    });
  });


  await Promise.all(erroredPackages.map(async (key) => {
    console.log(`retrying errored package: ${key}`);
    await createNodes(key);
    await createRelationships(key);
  }));
};
