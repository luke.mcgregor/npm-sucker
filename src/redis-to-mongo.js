const { MongoClient } = require('mongodb');
const Redis = require('ioredis');
const { sanitizePackage } = require('./packageSanitizer');

module.exports.run = async () => {
  const redis = new Redis(process.env.REDIS_PORT || 56379, process.env.REDIS_HOST || 'localhost');

  const mongoConnection = await MongoClient.connect(
    `mongodb://${process.env.MONGO_HOST || 'localhost'}:${process.env.MONGO_PORT || 57017}/npm`,
    { useNewUrlParser: true },
  );

  const mongoNpmDb = mongoConnection.db('npm');
  await mongoNpmDb.collection('packages').createIndex('name');
  const count = await mongoNpmDb.collection('packages').estimatedDocumentCount();
  if (count < 900000) {
    const updateMongo = async (key) => {
      try {
        const pkg = JSON.parse(await redis.get(key));
        const set = await sanitizePackage(pkg);
        await mongoNpmDb.collection('packages').updateOne({ name: pkg.name }, {
          $set: set,
        }, { upsert: true, safe: false });
      } catch (ex) {
        console.error(ex);
      }
    };
    let packageCount = 0;
    await new Promise((resolve) => {
      const stream = redis.scanStream({ match: 'package:*', count: 1000 });
      stream.on('data', (resultKeys) => {
        // Pause the stream from scanning more keys until we've migrated the current keys.
        stream.pause();

        Promise.all(resultKeys.map(updateMongo)).then(() => {
          // Resume the stream here.
          stream.resume();
        });
        packageCount += resultKeys.length;
        console.log('mongo: ', packageCount);
      });

      stream.on('end', () => {
        resolve();
      });
    });
  } else {
    console.log(`skipping mongo as it is already populated (${count})!`);
  }
  await mongoConnection.close();
};
