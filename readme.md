To run the sucker:
    `docker-compose up`


To store all that NPM in redis you will need to up the memory allowed for containers (see https://stackoverflow.com/a/44533437/1070291) 
When I ran it (early 2019) it used around 6GB of ram, I allocated 8GB